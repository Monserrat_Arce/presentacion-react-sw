import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Compo1 from './components/Compo1'
import Compo2 from './components/Compo2'
import Compo3 from './components/compo3'
import Compo4 from './components/Compo4'
import Compo5 from './components/Compo5'
import Compo6 from './components/Compo6'


function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <body>
        <Compo1></Compo1>
        <div class="d2">
          <Compo2></Compo2>
          <Compo3></Compo3>
          <Compo4></Compo4>
        </div>
        <div class="d3">
          <Compo5></Compo5>
          <Compo6></Compo6>
        </div>
      </body>

    </>
  )
}

export default App
