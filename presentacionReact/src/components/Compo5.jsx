import mascotas from '../assets/img/mascotas.png'
import compo5 from '../components/compo5.css'

function Compo5(){
    return(
        <>
            <div class="card" >
                <div class="center">
                    <img src={mascotas} class="card-img-top" style={{width: '150px', height: '140px'}}/>
                </div>

                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-title">Mascotas</h5>
                    </div>
                    <p class="card-text">Perros:  3.</p>
                    <p class="card-text">Conejos: 1.</p>
                </div>
            </div>
        </>
    )
}
export default Compo5