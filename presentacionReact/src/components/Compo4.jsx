import amor from '../assets/img/amor.png'
import compo4 from '../components/compo4.css'

function Compo4(){
    return(
        <>
            <div class="card" >
                <div class="center">
                    <img src={amor} class="card-img-top" style={{width: '150px', height: '140px'}}/>
                </div>

                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-title">Gustos personales</h5>
                    </div>
                    <p class="card-text">Color favorito:  negro.</p>
                    <p class="card-text">Animal favorito: conejo.</p>
                    <p class="card-text">Comida favorita: pastas</p>
                    <p class="card-text">Pelicula favorita: Forrest Gump.</p>
                </div>
            </div>
        </>
    )
}
export default Compo4