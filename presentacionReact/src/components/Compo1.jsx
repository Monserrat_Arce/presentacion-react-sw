import mons3 from '../assets/mons3.jpeg'
import compo1 from '../components/compo1.css'

function Compo1(){
    return(
        <>
            <div class="cont1">
                <div class="tittle">
                    <h1>Presentación</h1>
                </div>
            </div>

            <div class="img">
                <div>
                    <img src={mons3}  class="center" style={{width: '250px', height: '415px' }}/>
                </div>
                <div class="info">
                    <h2 >Nombre:</h2>
                    <h3> Arce Serrano Monserrat</h3>
                </div>
            </div>
        </>
    )
}
export default Compo1