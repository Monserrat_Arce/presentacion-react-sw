import usuario from '../assets/img/usuario.png'
import compo2 from '../components/compo2.css'
// import 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css';


function Compo2(){
    return(
        <>
            <div class="card" >
                <div class="center">
                <img src={usuario} class="card-img-top" style={{width: '150px', height: '140px'}}/>
                </div>

                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-title">Información personal</h5>
                    </div>
                    <p class="card-text">Domicilio: Yautepec, Morelos.</p>
                    <p class="card-text">Edad: 21.</p>
                    <p class="card-text">Telefono: 7352120436</p>
                </div>
            </div>
        </>
    )
}
export default Compo2